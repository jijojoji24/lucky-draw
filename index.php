<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>LUCKY DRAW COUNTER</title>
    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&family=Manrope:wght@200;300;400;500;600;700&family=Poppins:ital,wght@0,700;1,700&family=Prompt:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <!-- BOOTSTRAP LINK -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <!-- CSS LINK -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body >
    <section class="bg_section">
            <div class="bg_img" style="background-image: url(./assets/images/bgLight.png);">
                <div class="container">
                <div class="slot-machine">
                    
                    <div class="slot_heading Dglow position-relative" id="Dglow">
                       
                        <h1>WEEKLY WINNER 1/10</h1>
                        <div class="star_img">
                            <img src="./assets/images/stars.png" alt="">
                        </div>
                        <div class="winner_name">
                            <h1>John Doe</h1>
                        </div>
                        <div id="svg__00 " class="lottie"
                        data-animation-path="https://assets5.lottiefiles.com/packages/lf20_poy1vcfr.json"
                        data-autoplay="true" data-rederer="svg" data-anim-loop="true" data-name="Lottie Animation">
                    </div>
                        
                    </div>
                    <div class="slot_img">

                        <div class="main_parent_count">
                            <div class="row">
                                <div class="fancy" id="counter">
                                    <div class="lever" onclick="picture3()"; >
                                        <img src="./assets/images/Group 9547.png"  id="playFancy"  alt="">
                                    </div>
                                    <div class="wrapper">
                                        <ul class="slot">
                                            <li><span>0</span></li>
                                            <li><span>9</span></li>
                                            <li><span>8</span></li>
                                            <li><span>7</span></li>
                                            <li><span>6</span></li>
                                            <li><span>5</span></li>
                                            <li><span>4</span></li>
                                            <li><span>3</span></li>
                                            <li><span>2</span></li>
                                            <li><span>1</span></li>

                                        </ul>
                                    </div>

                                </div>
                            
                                <div class="startDraw" >
                                    <ul id="startDiv">
                                        <li>
                                            &nbsp;
                                        </li>
                                        <li>
                                            &nbsp;
                                        </li>
                                        <li>
                                            &nbsp;
                                        </li>
                                        <li>
                                            S
                                        </li>
                                        <li>
                                            T
                                        </li>
                                        <li>
                                            A
                                        </li>
                                        <li>
                                            R
                                        </li>
                                        <li>
                                            T
                                        </li>
                                        <li>
                                            >
                                        </li>
                                        <li>
                                            &nbsp;
                                        </li>
                                        <li>
                                            &nbsp;
                                        </li>
                                        <li>
                                            &nbsp;
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="car_img ">
                            <img src="./assets/images/car.png" id="bigpic" alt="" style="display:none;">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <audio id="a" controls  autoplay loop>
        <source src="./assets/images/beautiful-piano-music-relaxing-light-morning-relaxdaily-n090.mp3" type="audio/mpeg">
    </audio> 
  
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script> -->
    <script src="./assets/plugins/jquery1.7.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="./assets/plugins/jquery.easing.1.3.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/plugins/jquery.jSlots.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>

    <script>
        
        $('.fancy .slot').jSlots({
            number: 12,
            winnerNumber: 1,
            spinner: '#playFancy',
            easing: 'easeOutSine',
            time: 4000,
            loops: 3,
            onStart: function () {

                $('.slot').removeClass('winner').delay(900).fadeIn(1000);
            },
            onWin: function (winCount, winners) {


                $.each(winners, function () {
                    this.addClass('winner');
                });

                picture();
                picture2();
                $('#counter').addClass('newClass');
                $('#playFancy').addClass('leverClass');
                $('#svg__00').addClass('svgClass');

            }

        });
        function picture() {
            var pic = "./assets/images/car.png"
            // document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
            document.getElementById('bigpic').style.display = 'block';

        }

        function picture2() {
            document.getElementById("Dglow").style.opacity = '1';
        }
        function picture3() {
            document.getElementById("playFancy").style.transform = 'rotate(74deg)';
            document.getElementById("playFancy").style.transition = '1s ease all';
            $('.startDraw').addClass('hide');
            var myaudio = document.getElementById("a");
            var a = $("#a source").attr("src");
            myaudio.load(a);     
            myaudio.autoplay = true;
        }
    </script>
</body>
</html>